package com.eleksploded.eleklib.updatecheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.eleksploded.eleklib.ElekLib;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.loading.FMLEnvironment;

public class UpdateChecker {
	private static boolean ran = false;
	private static UpdateChecker instance;
		
	private UpdateChecker() {
		MinecraftForge.EVENT_BUS.register(this);
		instance = this;
	}
	
	public static void init() {
		if(!ran) {
			ran = true;
			new UpdateChecker();
		}
	}
	
	public static UpdateChecker instance() {
		return instance;
	}
	
	@SubscribeEvent
	public void onJoinWorld(EntityJoinWorldEvent e) {
		if(FMLEnvironment.dist == Dist.CLIENT && e.getEntity() instanceof PlayerEntity) {
			runUpdateChecks();
		}
	}
	
	@SubscribeEvent
	public void onWorldLoaded(WorldEvent.Load e) {
		if(FMLEnvironment.dist == Dist.DEDICATED_SERVER) {
			runUpdateChecks();
		}
	}
	
	public void runUpdateChecks() {
		@SuppressWarnings("unchecked")
		List<String> ignored = (ArrayList<String>) ElekLib.ElekLibConfig.getValue("ignoreUpdates");
		ModList.get().forEachModContainer((modid, container) -> {
			if(!ignored.contains(container.getModInfo().getDisplayName())) {
				ElekLib.debug("Running update checker for " + modid);
				Thread thread = new Thread(new UpdateCheckThread(container));
				thread.start();
			}
		});
	}
	
	public static void sendMessage(String message, String...args) {
		System.out.println(message + Arrays.toString(args));
		
		for(int i = 0; i < args.length; i++) {
			message = message.replace("$" + String.valueOf(i + 1), args[i]);
		}
		
		ElekLib.proxy.sendMessage(message);
	}

}
