package com.eleksploded.eleklib.updatecheck;

import com.eleksploded.eleklib.ElekLib;

import net.minecraftforge.fml.ModContainer;
import net.minecraftforge.fml.VersionChecker;
import net.minecraftforge.fml.VersionChecker.CheckResult;

public class UpdateCheckThread implements Runnable {

	private final ModContainer container;
	
	/**
	 * Should only be used by eleklib.
	 * Should only be called after init finishes
	 * @param modid
	 */
	public UpdateCheckThread(ModContainer c) {
		container = c;
	}
	
	@Override
	public void run() {
		CheckResult result = VersionChecker.getResult(container.getModInfo());
		
		switch(result.status) {
		
		case BETA_OUTDATED:
			ElekLib.LOGGER.info(ElekLib.ElekLibConfig.getString("betaOutdatedMessage"));
			UpdateChecker.sendMessage(ElekLib.ElekLibConfig.getString("betaOutdatedMessage"), container.getModInfo().getDisplayName(), result.target.getCanonical());
			break;
			
		case FAILED:
			ElekLib.LOGGER.info(ElekLib.ElekLibConfig.getString("failedMessage"));
			UpdateChecker.sendMessage(ElekLib.ElekLibConfig.getString("failedMessage"), container.getModInfo().getDisplayName());
			break;
			
		case OUTDATED:
			ElekLib.LOGGER.info(ElekLib.ElekLibConfig.getString("outdatedMessage"));
			UpdateChecker.sendMessage(ElekLib.ElekLibConfig.getString("outdatedMessage"), container.getModInfo().getDisplayName(), result.target.getCanonical());
			break;

		case PENDING:
			try {
				Thread.sleep(1000);
				run();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
			
		case BETA:
		case AHEAD:
		case UP_TO_DATE:
		default:
			break;
		}
	}
}
