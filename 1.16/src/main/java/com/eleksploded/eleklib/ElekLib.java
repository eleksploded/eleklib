package com.eleksploded.eleklib;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.eleksploded.eleklib.commands.OldCommandWrapper;
import com.eleksploded.eleklib.config.Config;
import com.eleksploded.eleklib.config.ConfigBuilder;
import com.eleksploded.eleklib.proxy.ClientProxy;
import com.eleksploded.eleklib.proxy.IProxy;
import com.eleksploded.eleklib.proxy.ServerProxy;
import com.eleksploded.eleklib.tests.ElekLibTests;
import com.eleksploded.eleklib.updatecheck.UpdateChecker;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("eleklib")
@Mod.EventBusSubscriber(modid = "eleklib")
public class ElekLib
{
	static boolean tests = true;
	
	public static final Logger LOGGER = LogManager.getLogger();
	
	public static IProxy proxy = DistExecutor.safeRunForDist(() -> ClientProxy::new, () -> ServerProxy::new);
	public static Config ElekLibConfig;
	
    public ElekLib() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::complete);
        MinecraftForge.EVENT_BUS.addListener(this::server);
        
        ElekLibConfig = ConfigBuilder.builder()
        		.enterCategory("ModValue", "Global Values to do with the mod")
        		.addBool("debug", false, "Enable debug mode")
        		.exitCategory()
        		.category("Log Cleanup", "Value for log cleanup module", b -> {
        			b.addBool("cleanup-logs", true, "Is the log cleanup module active?");
        			b.addInt("max-log", 5, "Number of log files to keep (This includes the current latest & debug)", 1, 64);
        			b.addBool("cleanup-crash", true, "Is the crash report cleanup module active?");
        			b.addInt("max-crash", 3, "Number of crash reports to keep", 1, 64);
        		})
        		.enterCategory("Commands", "Values to do with old command management")
        			.addBool("register-command", true, "Should oldstyle commands be registered")
        			.addBool("forceBuilder", false, "Should the selected builded be forced, even if a command overrides it?")
        			.addString("builder-version", "V1", "What version builder should be used? (Options: V1, V2)"
        				+ " See https://gitlab.com/eleksploded/eleklib/-/wikis/Command%20Wrapper for info on the versions")
        			.enterCategory("V2 Builder", "Value for the V2 Command Builder")
        				.addInt("maxArgs", 3, "Max number of args that can be autocompleted", 1, 10)
        			.exitCategory()
        		.exitCategory()
        		.category("Update Checker", "Messages for the update checker module", c -> {
        			c.addBool("updateCheckEnabled", true, "Enable/Disable the update checker module");
        			c.addValue("ignoreUpdates", new ArrayList<String>(), "Mod Display names to ignore for update checks");
        			c.addString("betaOutdatedMessage", "A new beta build ($2) is avalible for $1", "Message for an outdated beta build. Use $1 for the mod name. Use $2 for the new version");
        			c.addString("failedMessage", "There was an error checking for updates for $1", "Message for if the update checker fails. Use $1 for the mod name.");
        			c.addString("outdatedMessage", "A new update ($2) is avalible for $1", "Message for an outdated release build. Use $1 for the mod name. Use $2 for the new version");
        		})
        		.build("eleklib", ModConfig.Type.COMMON);
                
        if(tests) {
        	LOGGER.warn("Eleklib is running internal tests");
        	new ElekLibTests();
        }
    }
    
    private void server(final FMLServerStartingEvent e) {
    	ServerProxy.dataDir = e.getServer().getDataDirectory();
    }

    private void complete(final FMLLoadCompleteEvent event)
    {
    	LogCleanup.run();
    	UpdateChecker.init();
    }

    @SubscribeEvent
    public static void onServerStarting(RegisterCommandsEvent event) {
    	if(ElekLibConfig.getBool("register-command")) {
    		OldCommandWrapper.registerCommands(event.getDispatcher());
    	}
    }
    
    public static void debug(String message) {
    	if(ElekLibConfig.getBool("debug")) {
    		LOGGER.debug(message);
    	}
    }
}