package com.eleksploded.eleklib.commands;

import java.util.ArrayList;
import java.util.List;

import com.eleksploded.eleklib.ElekLib;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;

import net.minecraft.command.CommandException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.MessageArgument;
import net.minecraft.util.math.BlockPos;

public class OldCommandWrapper {

	static List<CommandBase> commands = new ArrayList<CommandBase>();

	public static void addOldCommand(CommandBase base) {
		commands.add(base);
		ElekLib.LOGGER.debug("Added legacy command: " + base.getName());
	}

	public static void registerCommands(CommandDispatcher<CommandSource> d) {

		String override = ElekLib.ElekLibConfig.getString("builder-version");

		for(CommandBase base : commands) {
			String builderUsed;
			String builder = ElekLib.ElekLibConfig.getBool("forceBuilder") ? override : base.getBuilder();
			
			if(builder.equalsIgnoreCase("V1")) {
				builderUsed = "V1";
				buildCommandV1(base);
			} else if((builder.equalsIgnoreCase("V2"))) {
				builderUsed = "V2";
				buildCommandV2(base);
			} else {
				builderUsed = "V1";
				buildCommandV1(base);
			}

			d.register(base.command);
			ElekLib.LOGGER.debug("Registered legacy command: '" + base.getName() + "' Using " + builderUsed + " builder.");
		}
	}

	static void buildCommandV2(CommandBase base) {
		base.command = Commands.literal(base.getName()).requires(s -> base.checkPermission(s.getServer(), s));

		base.command.executes(s -> {
			try {
				base.execute(s.getSource().getServer(), s.getSource(), new String[] {});
			} catch(CommandException e) {
				return 1;
			}
			return 0;
		});

		for(int i = 0; i < ElekLib.ElekLibConfig.getInt("maxArgs"); i++) {
			base.command.then(Commands.argument("arg-" + String.valueOf(i), StringArgumentType.string())
					.suggests((context, builder) -> {
						List<String> args = new ArrayList<String>();

						for(int j = 0; j < ElekLib.ElekLibConfig.getInt("maxArgs")-1; j++) {
							args.add(StringArgumentType.getString(context, "arg-" + String.valueOf(j)));
						}

						List<String> com = base.getTabCompletions(context.getSource().getServer(), context.getSource(), args.toArray(new String[] {}), new BlockPos(context.getSource().getPos()));
						return ISuggestionProvider.suggest(com, builder);
					}).executes(s -> {
				List<String> args = new ArrayList<String>();
				for(int k = 0; k < base.command.getArguments().size(); k++) {
					args.add(StringArgumentType.getString(s, "arg-" + String.valueOf(k)));
				}
				
				try {
					base.execute(s.getSource().getServer(), s.getSource(), args.toArray(new String[] {}));
				} catch(CommandException e) {
					return 1;
				}
				return 0;
				
			}));
		}
		
		base.command.then(Commands.argument("legacy-command", MessageArgument.message()).executes(s -> {
			
			List<String> args = new ArrayList<String>();
			for(int k = 0; k < base.command.getArguments().size(); k++) {
				args.add(StringArgumentType.getString(s, "arg-" + String.valueOf(k)));
			}
			
			String[] m = MessageArgument.getMessage(s, "legacy-command").getUnformattedComponentText().split(" ");

			for(String ms : m) {
				args.add(ms);
			}
			
			try {
				base.execute(s.getSource().getServer(), s.getSource(), args.toArray(new String[] {}));
			} catch(CommandException e) {
				return 1;
			}
			return 0;
		}));
	}

	static void buildCommandV1(CommandBase base) {
		base.command = Commands.literal(base.getName()).requires(s -> base.checkPermission(s.getServer(), s));
		base.command = base.command.executes(s -> {
			try {
				base.execute(s.getSource().getServer(), s.getSource(), new String[] {});
			} catch(CommandException e) {
				return 1;
			}
			return 0;
		});

		base.command = base.command.then(Commands.argument("legacy-command", MessageArgument.message()).executes(s -> {
			String[] args = MessageArgument.getMessage(s, "legacy-command").getUnformattedComponentText().split(" ");

			try {
				base.execute(s.getSource().getServer(), s.getSource(), args);
			} catch(CommandException e) {
				return 1;
			}
			return 0;
		}));
	}
}
