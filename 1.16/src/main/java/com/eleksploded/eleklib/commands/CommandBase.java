package com.eleksploded.eleklib.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.eleksploded.eleklib.ElekLib;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;

import net.minecraft.command.CommandException;
import net.minecraft.command.CommandSource;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public abstract class CommandBase {
	
	/**
	 * Used for building command. Do not mess with.
	 */
	public LiteralArgumentBuilder<CommandSource> command;
	
	public abstract String getName();
	
	public String getUsage(CommandSource sender) {
		return "Unknown Usage";
	}
	
	public String getBuilder() {
		return ElekLib.ElekLibConfig.getString("builder-version");
	}
	
	public int getRequiredPermissionLevel()
    {
        return 4;
    }
	
	public boolean checkPermission(MinecraftServer server, CommandSource sender)
    {
        return sender.hasPermissionLevel(this.getRequiredPermissionLevel());
    }

	@Nonnull
	public List<String> getAliases() {
		return new ArrayList<String>();
	}
	
	public List<String> getTabCompletions(MinecraftServer server, CommandSource sender, String[] args, @Nullable BlockPos targetPos) {
		return Collections.emptyList();
	}

	public abstract void execute(MinecraftServer server, CommandSource sender, String[] args) throws CommandException;
}
