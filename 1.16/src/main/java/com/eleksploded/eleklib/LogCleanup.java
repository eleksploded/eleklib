package com.eleksploded.eleklib;

import java.io.File;
import java.util.Arrays;

public class LogCleanup {

	static void run() {
		if(ElekLib.ElekLibConfig.getBool("cleanup-logs")) {
			cleanLog();
		}

		if(ElekLib.ElekLibConfig.getBool("cleanup-crash")) {
			cleanCrash();
		}
	}

	private static void cleanLog() {
		File dir = ElekLib.proxy.getGameFolder("logs");
		File[] files = sortFiles(dir);
		
		if(files == null) {
			return;
		}

		int n = ElekLib.ElekLibConfig.getInt("max-log");

		for (int i = 0; i < (files.length - n); i++) {
			files[i].delete();
		}
	}

	private static void cleanCrash() {
		File dir = ElekLib.proxy.getGameFolder("crash-reports");
		File[] files = sortFiles(dir);
		
		if(files == null) {
			return;
		}

		int n = ElekLib.ElekLibConfig.getInt("max-crash");

		for (int i = 0; i < (files.length - n); i++) {
			files[i].delete();
		}
	}

	private static File[] sortFiles(File dir) {
		File[] files = dir.listFiles();
		
		if(files == null || files.length == 0 ) {
			return null;
		}
		
		Pair[] pairs = new Pair[files.length];
		for (int i = 0; i < files.length; i++)
			pairs[i] = new Pair(files[i]);

		Arrays.sort(pairs);

		for (int i = 0; i < files.length; i++)
			files[i] = pairs[i].f;

		pairs = null;
		return files;
	}

	@SuppressWarnings("rawtypes")
	private static class Pair implements Comparable {
		public long t;
		public File f;

		public Pair(File file) {
			f = file;
			t = file.lastModified();
		}

		public int compareTo(Object o) {
			long u = ((Pair) o).t;
			return t < u ? -1 : t == u ? 0 : 1;
		}
	};
}
