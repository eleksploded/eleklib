package com.eleksploded.eleklib.registries;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import com.eleksploded.eleklib.ElekLib;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = "eleklib", bus = Mod.EventBusSubscriber.Bus.MOD)
public class RetroGameRegistry {

	public static List<Item> items = new ArrayList<Item>();
	public static List<Block> blocks = new ArrayList<Block>();

	public static void registerItem(Item item, String name) {

		if(item.getRegistryName() != null) {
			ElekLib.LOGGER.warn("Skipping name on " + name + " since it has a predefined registry name");
			registerItem(item);
			return;
		}

		if(!isValidNamespace(name)) {
			ElekLib.LOGGER.error("Skipping " + name + " due to invalid name");
			return;
		}

		ResourceLocation reg;
		if(!name.contains(":")) {
			ElekLib.LOGGER.warn("Registering " + name + " without a modid reference. This means its using the 'minecraft:' prefix");
			reg = new ResourceLocation(name);
		} else {
			String[] n = name.split(":");
			if(n.length != 2) {
				ElekLib.LOGGER.error("Skipping " + name + " due to invalid name");
			}
			reg = new ResourceLocation(n[0], n[1]);
		}

		item.setRegistryName(reg);
		items.add(item);
	}

	public static void registerItem(Item item) {
		System.out.println("Bleh");
		if(item.getRegistryName() != null) {
			items.add(item);
		} else {
			ElekLib.LOGGER.error("Skipping " + item.getClass().getName() + " due to not having a name");
		}
	}
	
	public static void registerBlock(Block block, String name, @Nullable ItemGroup group) {

		if(block.getRegistryName() != null) {
			ElekLib.LOGGER.warn("Skipping name on " + name + " since it has a predefined registry name");
			registerBlock(block);
			return;
		}

		if(!isValidNamespace(name)) {
			ElekLib.LOGGER.error("Skipping " + name + " due to invalid name");
			return;
		}

		ResourceLocation reg;
		if(!name.contains(":")) {
			ElekLib.LOGGER.warn("Registering " + name + " without a modid reference. This means its using the 'minecraft:' prefix");
			reg = new ResourceLocation(name);
		} else {
			String[] n = name.split(":");
			if(n.length != 2) {
				ElekLib.LOGGER.error("Skipping " + name + " due to invalid name");
			}
			reg = new ResourceLocation(n[0], n[1]);
		}

		block.setRegistryName(reg);
		blocks.add(block);

		if(group != null) {
			BlockItem bi = new BlockItem(block, new Item.Properties().group(group));
			bi.setRegistryName(block.getRegistryName());
			registerItem(bi);
		}
	}

	public static void registerBlock(Block block, @Nullable ItemGroup group) {
		if(block.getRegistryName() != null) {
			blocks.add(block);

			if(group != null) {
				BlockItem bi = new BlockItem(block, new Item.Properties().group(group));
				bi.setRegistryName(block.getRegistryName());
				registerItem(bi);
			}
		} else {
			ElekLib.LOGGER.error("Skipping " + block.getClass().getName() + " due to not having a name");
		}
	}

	public static void registerBlock(Block block) {
		registerBlock(block, null);
	}

	private static boolean isCharValid(char c) {
		return c == ':' || c == '_' || c == '-' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '.';
	}

	private static boolean isValidNamespace(String namespaceIn) {
		for(int i = 0; i < namespaceIn.length(); ++i) {
			if (!isCharValid(namespaceIn.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	@SubscribeEvent
	public static void block(RegistryEvent.Register<Block> e) {
		blocks.forEach(block -> {
			if(ElekLib.ElekLibConfig.getBool("debug")) {
				ElekLib.LOGGER.debug("Registering block: " + block.getRegistryName().getPath());
			}
			e.getRegistry().register(block);
		});
	}

	@SubscribeEvent
	public static void item(RegistryEvent.Register<Item> e) {		
		items.forEach(item -> {
			if(ElekLib.ElekLibConfig.getBool("debug")) {
				ElekLib.LOGGER.debug("Registering item: " + item.getRegistryName().getPath());
			}
			e.getRegistry().register(item);
		});
	}

}