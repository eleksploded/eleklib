package com.eleksploded.eleklib.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.common.ForgeMod;

public class PlayerUtils {
	public static boolean isPlayerOpped(MinecraftServer server, PlayerEntity player) {
		return server.getPlayerList().getOppedPlayers().getEntry(player.getGameProfile()) != null;
	}
	
	public static RayTraceResult traceLook(PlayerEntity player) {
		return traceLook(player, player.getAttribute(ForgeMod.REACH_DISTANCE.get()).getValue());
	}

	public static RayTraceResult traceLook(PlayerEntity player, double range) {
		if (player != null) {
			if (player.getEntityWorld() != null) {
				double d0 = range;
				RayTraceResult rtr = player.pick(d0, 0, false);
				Vector3d vec3d = player.getEyePosition(0);
				boolean flag = false;
				double d1 = d0;
				if (player.isCreative()) {
					d1 = 6.0D;
					d0 = d1;
				} else {
					if (d0 > 3.0D) {
						flag = true;
					}
				}
				
				d1 = d1 * d1;
				if (rtr != null) {
					d1 = rtr.getHitVec().squareDistanceTo(vec3d);
				}

				Vector3d vec3d1 = player.getLook(1.0F);
				Vector3d vec3d2 = vec3d.add(vec3d1.x * d0, vec3d1.y * d0, vec3d1.z * d0);
				AxisAlignedBB axisalignedbb = player.getBoundingBox().expand(vec3d1.scale(d0)).grow(1.0D, 1.0D, 1.0D);
				EntityRayTraceResult entityraytraceresult = ProjectileHelper.rayTraceEntities(player.getEntityWorld(), player, vec3d, vec3d2, axisalignedbb, (p_215312_0_) -> {
					return !p_215312_0_.isSpectator() && p_215312_0_.canBeCollidedWith();
				});
				if (entityraytraceresult != null) {
					Vector3d vec3d3 = entityraytraceresult.getHitVec();
					double d2 = vec3d.squareDistanceTo(vec3d3);
					if (flag && d2 > 9.0D) {
						rtr = BlockRayTraceResult.createMiss(vec3d3, Direction.getFacingFromVector(vec3d1.x, vec3d1.y, vec3d1.z), new BlockPos(vec3d3));
					} else if (d2 < d1 || rtr == null) {
						rtr = entityraytraceresult;
					}
				}
				return rtr;
			}
		}
		return null;
	}
}
