package com.eleksploded.eleklib.entity;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;

public class EntityUtils {
	
	public static BlockPos getBlockPos(Entity entity) {
		return new BlockPos(entity.getPositionVec());
	}
	
}
