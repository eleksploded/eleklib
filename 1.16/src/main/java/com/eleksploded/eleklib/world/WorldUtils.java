package com.eleksploded.eleklib.world;

import javax.annotation.Nullable;

import com.eleksploded.eleklib.ElekLib;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;

public class WorldUtils {

	public static LightningBoltEntity spawnLightningBolt(World world, BlockPos pos, @Nullable ServerPlayerEntity caster) {
		if(world.isRemote) {
			StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
			StackTraceElement e = stacktrace[2];
			String methodName = e.getMethodName();			
			ElekLib.LOGGER.error("spawnLightningBolt is being called on client by " + methodName + " at line " + e.getLineNumber());
			return null;
		} else {
			LightningBoltEntity l = EntityType.LIGHTNING_BOLT.create(world);
			l.func_233576_c_(Vector3d.func_237492_c_(pos));
			if(caster != null) l.setCaster(caster);
			world.addEntity(l);
			return l;
		}
	}
}
