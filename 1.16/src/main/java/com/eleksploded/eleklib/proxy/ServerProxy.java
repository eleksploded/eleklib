package com.eleksploded.eleklib.proxy;

import java.io.File;

import com.eleksploded.eleklib.ElekLib;
import com.eleksploded.eleklib.entity.PlayerUtils;
import com.eleksploded.eleklib.tests.ElekLibTests;

import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.event.TickEvent.PlayerTickEvent;

public class ServerProxy implements IProxy {

	public static File dataDir;
	
	@Override
	public File getGameFolder(String folder) {
		return new File(dataDir, folder);
	}

	//Tests
	
	@Override
	public void playerTick(PlayerTickEvent e) {
		RayTraceResult r = PlayerUtils.traceLook(e.player);
		
		if(r instanceof BlockRayTraceResult) {
			ElekLibTests.l.debug(((BlockRayTraceResult)r).getPos());
		} else if(r instanceof EntityRayTraceResult) {
			ElekLibTests.l.debug(((EntityRayTraceResult)r).getEntity());
		}
	}

	@Override
	public void sendMessage(String message) {
		if(message == null) return;
		ElekLib.LOGGER.info(message);
	}
}
