package com.eleksploded.eleklib.proxy;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.event.TickEvent;

public class ClientProxy implements IProxy {

	@SuppressWarnings("resource")
	@Override
	public File getGameFolder(String folder) {
		return new File(Minecraft.getInstance().gameDir, folder);
	}
	
	//Tests
	public void playerTick(TickEvent.PlayerTickEvent e) {
		
	}
	
	@SuppressWarnings("resource")
	public void sendMessage(String message) {
		if(message == null) return;
		PlayerEntity player = Minecraft.getInstance().player;
		player.sendMessage(new StringTextComponent(message), player.getUniqueID());
	}

}
