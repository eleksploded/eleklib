package com.eleksploded.eleklib.proxy;

import java.io.File;

import net.minecraftforge.event.TickEvent;

public interface IProxy {
	File getGameFolder(String folder);
	
	//Tests
	
	public void playerTick(TickEvent.PlayerTickEvent e);
	public void sendMessage(String message);
}
