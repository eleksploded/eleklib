package com.eleksploded.eleklib.tests;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.eleksploded.eleklib.ElekLib;
import com.eleksploded.eleklib.commands.OldCommandWrapper;
import com.eleksploded.eleklib.config.Config;
import com.eleksploded.eleklib.config.ConfigBuilder;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class ElekLibTests {
	static Config c;
	public static final Logger l = LogManager.getLogger("ElekLibTests");
	public ElekLibTests() {
		IEventBus ModBus = FMLJavaModLoadingContext.get().getModEventBus();
		ModBus.addListener(this::clientSetup);
		
		MinecraftForge.EVENT_BUS.register(this);
		
		OldCommandWrapper.addOldCommand(new TestLegacyCommand());
    	RegistryTest.register();
		
		c = ConfigBuilder.builder().enterCategory("GeneralC", "general values")
				.addBool("go", false, "Do go?")
				.exitCategory()
				.addInt("run", 5, "Runs", 1, 134)
				.addString("someString", "boo", "Boo!")
				.build("eleklib", ModConfig.Type.COMMON, "eleklib-test");
	}
	
	private void clientSetup(final FMLClientSetupEvent e) {
	}
	
	@SubscribeEvent
	public void playerTick(TickEvent.PlayerTickEvent e) {
		ElekLib.proxy.playerTick(e);
	}
}
