package com.eleksploded.eleklib.tests;

import java.util.ArrayList;
import java.util.List;

import com.eleksploded.eleklib.commands.CommandBase;

import net.minecraft.command.CommandException;
import net.minecraft.command.CommandSource;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;

public class TestLegacyCommand extends CommandBase {

	@Override
	public List<String> getTabCompletions(MinecraftServer server, CommandSource sender, String[] args,
			BlockPos targetPos) {
		
		List<String> complete = new ArrayList<String>();
		
		if(args[0] != null && args[0].startsWith("ba")) {
			complete.add("baan");
			complete.add("banana");
		}
		
		if(args[1] != null) {
			complete.add("1baan");
			complete.add("1banana");
		}
		
		return complete;
	}

	@Override
	public String getName() {
		return "legacytext";
	}

	@Override
	public void execute(MinecraftServer server, CommandSource sender, String[] args) throws CommandException {
		String msg = "";
		
		if(args.length == 0) {
			sender.sendErrorMessage(new StringTextComponent("Please specify a string"));
			return;
		}
		
		for(String arg : args) {
			msg = msg + arg + " ";
		}
		
		sender.sendFeedback(new StringTextComponent(msg), true);
	}

}
