package com.eleksploded.eleklib.tests;

import com.eleksploded.eleklib.registries.RetroGameRegistry;

import net.minecraft.item.Item;

public class RegistryTest {
	public static final Item testItem = new TestItem();
	
	public static void register() {
		RetroGameRegistry.registerItem(testItem);
	}
}
