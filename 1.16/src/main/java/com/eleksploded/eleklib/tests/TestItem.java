package com.eleksploded.eleklib.tests;

import net.minecraft.item.Item;

public class TestItem extends Item {

	public TestItem() {
		super(new Item.Properties());
		
		this.setRegistryName("eleklib", "testitem");
	}
	
}
