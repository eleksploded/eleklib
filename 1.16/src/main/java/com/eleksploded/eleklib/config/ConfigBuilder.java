package com.eleksploded.eleklib.config;

import java.util.function.Consumer;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModContainer;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.config.ModConfig;

public class ConfigBuilder {
	
	public static ConfigBuilder builder() {
		return new ConfigBuilder();
	}
	
	ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
	Config config = new Config();
	
	public ConfigBuilder enterCategory(String name, String desc) {
		builder.comment(desc).push(name);
		return this;
	}
	
	public ConfigBuilder category(String name, String desc, Consumer<ConfigBuilder> c) {
		this.enterCategory(name, desc);
		c.accept(this);
		this.exitCategory();
		return this;
	}
	
	public ConfigBuilder exitCategory() {
		builder.pop();
		return this;
	}
	
	public ConfigBuilder addBool(String key, boolean defaultValue, String desc) {
		ForgeConfigSpec.BooleanValue spec = builder.comment(desc).define(key, defaultValue);
		config.bools.put(key, spec);
		return this;
	}
	
	public ConfigBuilder addLong(String key, Long defaultValue, String desc, Long min, Long max) {
		ForgeConfigSpec.LongValue spec = builder.comment(desc).defineInRange(key, defaultValue, min, max);
		config.longs.put(key, spec);
		return this;
	}
	
	public ConfigBuilder addInt(String key, Integer defaultValue, String desc, Integer min, Integer max) {
		ForgeConfigSpec.IntValue spec = builder.comment(desc).defineInRange(key, defaultValue, min, max);
		config.ints.put(key, spec);
		return this;
	}
	
	public ConfigBuilder addDouble(String key, Double defaultValue, String desc, Double min, Double max) {
		ForgeConfigSpec.DoubleValue spec = builder.comment(desc).defineInRange(key, defaultValue, min, max);
		config.doubles.put(key, spec);
		return this;
	}
	
	public ConfigBuilder addString(String key, String defaultValue, String desc) {
		ForgeConfigSpec.ConfigValue<String> spec = builder.comment(desc).define(key, defaultValue);
		config.strings.put(key, spec);
		return this;
	}
	
	public <T> ConfigBuilder addValue(String key, T defaultValue, String desc) {
		ForgeConfigSpec.ConfigValue<T> spec = builder.comment(desc).define(key, defaultValue);
		config.values.put(key, spec);
		return this;
	}
	
	public Config build(String modId, ModConfig.Type type) {
		ModContainer c = ModList.get().getModContainerById(modId).orElseThrow(() -> new RuntimeException("Attempted to build config for an invalid modId"));
		c.addConfig(new ModConfig(type, builder.build(), c));
		return config;
	}
	
	public Config build(String modId, ModConfig.Type type, String filename) {
		
		if(!filename.endsWith(".toml")) {
			filename = filename.concat(".toml");
		}
		
		ModContainer c = ModList.get().getModContainerById(modId).orElseThrow(() -> new RuntimeException("Attempted to build config for an invalid modId"));
		c.addConfig(new ModConfig(type, builder.build(), c, filename));
		return config;
	}
}
