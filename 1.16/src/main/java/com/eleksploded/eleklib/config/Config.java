package com.eleksploded.eleklib.config;

import java.util.HashMap;
import java.util.Map;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
import net.minecraftforge.common.ForgeConfigSpec.DoubleValue;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.common.ForgeConfigSpec.LongValue;

public class Config {
	Map<String, ForgeConfigSpec.BooleanValue> bools = new HashMap<String, BooleanValue>();
	Map<String, ForgeConfigSpec.DoubleValue> doubles = new HashMap<String, DoubleValue>();
	Map<String, ForgeConfigSpec.IntValue> ints = new HashMap<String, IntValue>();
	Map<String, ForgeConfigSpec.LongValue> longs = new HashMap<String, LongValue>();
	Map<String, ForgeConfigSpec.ConfigValue<String>> strings = new HashMap<String, ConfigValue<String>>();
	
	@SuppressWarnings("rawtypes")
	Map<String, ForgeConfigSpec.ConfigValue> values = new HashMap<String, ConfigValue>();
	
	
	/**
	 * Careful... There be monsters
	 * 
	 * @param key
	 * @return value of key
	 */
	public Object getValue(String key) {
		return values.get(key).get();
	}
	
	/** More Monsters!!
	 * @param key
	 * @param o
	 */
	@SuppressWarnings("unchecked")
	public void writeValue(String key, Object o) {
		values.get(key).set(o);
		saveAll();
	}
	
	public String getString(String key) {
		return strings.get(key).get();
	}
	
	public boolean getBool(String key) {
		return bools.get(key).get();
	}
	
	public Double getDouble(String key) {
		return doubles.get(key).get();
	}
	
	public Integer getInt(String key) {
		return ints.get(key).get();
	}
	
	public Long getLong(String key) {
		return longs.get(key).get();
	}
	
	public void writeBool(String key, boolean value) {
		bools.get(key).set(value);
		saveAll();
	}
	
	public void writeDouble(String key, Double value) {
		doubles.get(key).set(value);
		saveAll();
	}
	
	public void writeInt(String key, Integer value) {
		ints.get(key).set(value);
		saveAll();
	}
	
	public void writeLong(String key, Long value) {
		longs.get(key).set(value);
		saveAll();
	}
	
	public void writeSteing(String key, String value) {
		strings.get(key).set(value);
		saveAll();
	}
	
	public void saveAll() {
		values.forEach((k,c) -> c.save());
		bools.forEach((k,c) -> c.save());
		ints.forEach((k,c) -> c.save());
		doubles.forEach((k,c) -> c.save());
		longs.forEach((k,c) -> c.save());
		strings.forEach((k,c) -> c.save());
	}
}
