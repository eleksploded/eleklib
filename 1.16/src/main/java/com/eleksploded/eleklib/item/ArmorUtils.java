package com.eleksploded.eleklib.item;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.eleksploded.eleklib.ElekLib;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ArmorUtils {
	
	
	/** Class should have constructor with params (ArmorMaterial, EquipmentSlotType)
	 * @param <T>
	 * @param c
	 * @return Array of armor in [head, chest, legs, boots] format
	 */
	@SuppressWarnings("unchecked")
	public static <T extends ArmorItem> T[] createFullArmorSet(Class<T> c, ArmorMaterial mat) {
		try {
			Constructor<T> con = c.getConstructor(ArmorMaterial.class, EquipmentSlotType.class);
			
			T head = con.newInstance(mat, EquipmentSlotType.HEAD);
			T chest = con.newInstance(mat, EquipmentSlotType.CHEST);
			T legs = con.newInstance(mat, EquipmentSlotType.LEGS);
			T boots = con.newInstance(mat, EquipmentSlotType.FEET);
			
			List<T> l = new ArrayList<T>();
			l.add(head);
			l.add(chest);
			l.add(legs);
			l.add(boots);
			
			return (T[]) l.toArray();
			
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			ElekLib.LOGGER.error("Failed to make isntances of " + c.getName()+ ". Please check on the contructor. Returning null");
			return null;
		}
	}
	
	public static boolean isFullSet(PlayerEntity player, Item headIn, Item chestIn, Item legsIn, Item bootsIn) {
		ItemStack boots = player.inventory.armorInventory.get(0);
		ItemStack pants = player.inventory.armorInventory.get(1);
		ItemStack chest = player.inventory.armorInventory.get(2);
		ItemStack head  = player.inventory.armorInventory.get(3);

		if(isFullArmor(player)){
			Item Itemboots = boots.getItem();
			Item Itempants = pants.getItem();
			Item Itemchest = chest.getItem();
			Item Itemhead = head.getItem();

			if(Itemboots == bootsIn && Itempants == legsIn && Itemchest == chestIn && Itemhead == headIn)
			{
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public static boolean isFullSet(PlayerEntity player, Item[] set) {
		return isFullSet(player, set[0],set[1],set[2],set[3]);
	}
	
	public static boolean isFullArmor(PlayerEntity player) {
		ItemStack boots = player.inventory.armorInventory.get(0);
		ItemStack pants = player.inventory.armorInventory.get(1);
		ItemStack chest = player.inventory.armorInventory.get(2);
		ItemStack head  = player.inventory.armorInventory.get(3);
		
		if(boots != ItemStack.EMPTY && pants != ItemStack.EMPTY && chest != ItemStack.EMPTY && head != ItemStack.EMPTY){
			return true;
		} else {
			return false;
		}
	}
}
